import React, {useContext, useEffect, useState} from "react";
import api from "../api";
import MovieCarousel from "../components/MovieCarousel";
import {Context} from "../App";

function History() {
    let [movies, setMovies] = useState([]);

    let { user } = useContext(Context);

    useEffect(() => {
        async function fetchWatched() {
            let movies = await api.fetchAll();

            let watched;
            if (user !== null) watched = await api.fetchWatched(user);
            else watched = JSON.parse(localStorage.getItem('watched') || '[]');

            setMovies(movies.filter(m => watched.includes(m.id)))
        }

        fetchWatched();
    }, [user]);

    return <>
        <div className="container">
            <h1 className={'title'}>Watched Videos</h1>
            {
                movies.length === 0
                    ? <h2>You have not watched any video yet.</h2>
                    : <MovieCarousel movies={movies}/>
            }
        </div>
    </>
}

export default History;