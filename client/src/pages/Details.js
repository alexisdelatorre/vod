import React from "react";
import {Link, useParams} from "react-router-dom";
import api from "../api";

function Details() {
    let { id } = useParams();
    let [movie, loading] = api.useMovie(id);

    return  (
        <div className="container">
            {
                loading
                    ? <h1>Loading...</h1>
                    : <>
                        <div className="details-container">
                            <img src={movie.poster} alt={movie.title}/>
                            <div className={'details-content'}>
                                <h1>{movie.title}</h1>
                                <p className={'ratings'}>Parental Rating: {movie.ratings.join(', ')} | Language: {movie.audios.join(', ')}</p>
                                <p className={'description'}>{movie.description}</p>
                                <p>Credits: {movie.credits.map(c => `${c.role} ${c.name}`).join(', ')}</p>
                                <Link to={`/video/${movie.id}`}><button>Watch Video</button></Link>
                            </div>
                        </div>
                    </>
            }
        </div>
    )
}

export default Details;