import React, {useContext} from "react";
import { useParams } from 'react-router-dom';
import api from '../api';
import {addToStrgArray} from "../util";
import {Context} from "../App";

function Video() {
    let { user } = useContext(Context);

    let { id } = useParams();
    let [movie, loading] = api.useMovie(id);

    let handleEnded = () => {
        if (user === null) addToStrgArray('watched', movie.id);
        else api.setWatched(movie.id, user);
    };

    return (
        loading ? <h1>Loading...</h1> :
            <div>
                <video
                    src={movie.video}
                    controls
                    onEnded={handleEnded}>
                </video>
            </div>
    )
}

export default Video;