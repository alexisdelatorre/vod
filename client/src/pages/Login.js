import React, {useContext, useState} from "react";
import api from "../api";
import {Context} from "../App";
import {useHistory} from 'react-router-dom'

function Login() {
    let [username, setUsername] = useState('');
    let [password, setPassword] = useState('');
    let [error, setError] = useState(null);

    let { setUser } = useContext(Context);

    let history = useHistory();

    let handleSubmit = e => {
        e.preventDefault();

        api.login(username, password)
            .then(userId => {
                setError(null);
                setUser(userId);
                localStorage.setItem('user', userId);
                history.push('/')
            })
            .catch(err => {
                if (err.response && err.response.status === 401) {
                    setError('Wrong username or password');
                }
            })
    };

    return <div id={'form-container'}>
        <div>
            <form onSubmit={handleSubmit}>
                <label htmlFor="username">Username</label><br/>
                <input type="text" name={'username'} value={username} onChange={e => setUsername(e.target.value)}/><br/><br/>
                <label htmlFor="password">Password</label><br/>
                <input type="password" name={'password'} value={password} onChange={e => setPassword(e.target.value)}/><br/><br/>
                <button type={'submit'}>Log In</button>
            </form>
            {error && <div>{error}</div>}
        </div>
    </div>

}

export default Login;