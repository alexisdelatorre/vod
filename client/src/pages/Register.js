import React, {useContext, useState} from "react";
import {Context} from "../App";
import {Link, useHistory} from "react-router-dom";
import api from "../api";

function Register() {
    let [username, setUsername] = useState('');
    let [password, setPassword] = useState('');
    let [repassword, setRepassword] = useState('');

    let [error, setError] = useState(null);
    let [success, setSuccess] = useState(false);

    let history = useHistory();

    let handleSubmit = e => {
        e.preventDefault();

        if (password !== repassword) {
            setError('Passwords do not match');
            return;
        }

        if (username === '' || password === '') {
            setError('Fields cannot be empty');
            return;
        }

        api.createUser(username, password)
            .then(() => {
                setSuccess(true);
            })
            .catch(err => {
                if (err.response && err.response.status >= 400) {
                    setError(err.response.data);
                }
            })
    };

    return <div id={'form-container'}>
        <div>
            {
                success
                    ? <p>User successfully registered, you can now <Link to={'/login'}>Log In</Link></p>
                    : <>
                        <form onSubmit={handleSubmit}>
                            <label htmlFor="username">Username</label><br/>
                            <input type="text" name={'username'} value={username} onChange={e => setUsername(e.target.value)}/><br/><br/>
                            <label htmlFor="password">Password</label><br/>
                            <input type="password" name={'password'} value={password} onChange={e => setPassword(e.target.value)}/><br/><br/>
                            <label htmlFor="repassword">Repeat Password</label><br/>
                            <input type="password" name={'repassword'} value={repassword} onChange={e => setRepassword(e.target.value)}/><br/><br/>
                            <button type={'submit'}>Register</button>
                        </form>
                        {error && <div>{error}</div>}
                    </>
            }
        </div>
    </div>
}

export default Register;