import React, {useEffect, useState} from 'react';
import api from '../api';
import MovieCarousel from '../components/MovieCarousel';

function Home() {
    let [movies, setMovies] = useState([]);

    useEffect(() => {
        api.fetchAll().then(setMovies);
    }, []);


    return (
        <div>
            <div className={'container'}><h1 className={'title'}>Movies</h1></div>
            <MovieCarousel movies={movies}/>
        </div>
    );
}

export default Home;
