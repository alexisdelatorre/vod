import './movieCarousel.css';

import React, {useEffect, useState} from "react";
import {filledArray, rotateL, rotateR} from "../util";
import {Link, useHistory} from "react-router-dom";

function MovieCarousel({ movies }) {
    // Current selected movie (the one in the middle)
    let [selected, setSelected] = useState(0);

    // This order is only visual, for scrolling and keyboard selection
    let [order, setOrder] = useState([]);

    // Prevents double key presses from messing up the order
    let [pressed, setPressed] = useState(null);

    let [screenW, setScreenW] = useState(window.innerWidth);
    let [carouselSize, setCarouselSize] = useState(0);
    let [middleSlide, setMiddleSlide] = useState(0);

    let history = useHistory();

    let goForward = () => {
        setOrder(rotateR(order));

        if (selected + 1 >= movies.length) setSelected(0);
        else setSelected(selected + 1);
    };

    let goBack = () => {
        setOrder(rotateL(order));

        if (selected <= 0) setSelected(movies.length - 1);
        else setSelected(selected - 1);
    };

    useEffect(() => {
        // prevents another element from intercepting the enter keypress
        document.activeElement.blur();
    }, []);

    useEffect(() => {
        let handleKeyDown = e => {
            if (pressed === null) {
                setPressed(e.key);
                if (e.key === 'ArrowRight') goForward();
                if (e.key === 'ArrowLeft') goBack();
                if (e.key === 'Enter') history.push(`/details/${movies[selected].id}`);
            }
        };

        let handleKeyUp = e => {
            if (e.key === pressed) setPressed(null);
        };

        document.addEventListener('keydown', handleKeyDown);
        document.addEventListener('keyup', handleKeyUp);

        return () => {
            document.removeEventListener('keydown', handleKeyDown);
            document.removeEventListener('keyup', handleKeyUp)
        };
    });

    useEffect(() => {
        let handleResize = () => setScreenW(window.innerWidth);
       window.addEventListener('resize', handleResize);
       return () => window.removeEventListener('resize', handleResize);
    }, []);

    useEffect(() => {
        // Rotate to put first movie in the middle of the carousel
        setOrder(rotateL(filledArray(movies.length), middleSlide - 1));
        setSelected(0);
    }, [movies, middleSlide]);

    // Calculate the size of the carousel
    useEffect(() => {
        let calcSize = n => (n * 200) + ((n - 1) * 30) + 50;
        let setSize = n => {
            setCarouselSize(calcSize(n));
            setMiddleSlide(Math.ceil(n / 2));
        };

        let i = 0;
        while (true) {
            i++;

            if (i >= movies.length) {
                setSize(i);
                break;
            }

            let currSize = calcSize(i);
            if (currSize > screenW - 300) {
                setSize(i -1);
                break;
            }
        }
    }, [screenW, movies]);

    return (
        <div id={'carousel'}>
            <div className={'control'}><button onClick={goBack}>{'◀'}</button></div>
            <div className={'slides'}>
                <ul style={{maxWidth: carouselSize}}>
                    {movies.map((m, i) => (
                        <li
                            className={'slide'}
                            style={{
                                order: order[i],
                                width: order[i] === middleSlide - 1 ? 250 : 200,
                                height: order[i] === middleSlide - 1 ? 350 : 300,
                            }}
                            key={m.id}
                        >
                            <div className="slide-content">
                                <Link to={`/details/${m.id}`}>
                                    <div
                                        className={'poster'}
                                        style={{
                                            backgroundImage: `url(${m.poster})`,
                                            width: order[i] === middleSlide - 1 ? 250 : 200,
                                            height: order[i] === middleSlide - 1 ? 350 : 300,
                                        }}
                                    />
                                    <p>{m.title}</p>
                                </Link>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
            <div className={'control'}><button onClick={goForward}>{'▶'}</button></div>
        </div>
    )
}

export default MovieCarousel;