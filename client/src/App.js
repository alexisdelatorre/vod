import './app.css';

import React, {useState} from "react";
import Home from './pages/Home';
import Details from './pages/Details';
import Video from './pages/Video';
import {BrowserRouter, HashRouter, Link, Route, Switch} from "react-router-dom";
import History from "./pages/History";
import Login from "./pages/Login";
import Register from "./pages/Register";

export const Context = React.createContext();

function App() {
    let [user, setUser] = useState(localStorage.getItem('user'));

    let handleLogout = () => {
      setUser(null);
      localStorage.removeItem('user');
    };

    return (
        <Context.Provider value={{ user, setUser }}>
            <BrowserRouter>
                <div>
                    <nav>
                        <div className="nav-content">
                            <h1>VOD Aplication</h1>
                            <ul>
                                <li><Link to={'/'}><button>Home</button></Link></li>
                                <li><Link to={'/history'}><button>History</button></Link></li>
                                {
                                    user === null
                                        ? <li><Link to={'/login'}><button>Log In</button></Link></li>
                                        : <li><button onClick={handleLogout}>Log Out</button></li>
                                }
                                { user === null && <li><Link to={'/register'}><button>Register</button></Link></li> }
                                <li className={'user'}>{user || 'Visitor'}</li>
                            </ul>
                        </div>
                    </nav>

                    <Switch>
                        <Route path={'/details/:id'}><Details/></Route>
                        <Route path={'/video/:id'}><Video/></Route>
                        <Route path={'/history'}><History/></Route>
                        <Route path={'/login'}><Login/></Route>
                        <Route path={'/register'}><Register/></Route>
                        <Route path={'/'}><Home/></Route>
                    </Switch>
                </div>
            </BrowserRouter>
        </Context.Provider>
    )
}

export default App;