// https://github.com/lodash/lodash/issues/2173#issuecomment-406597580
export function rotateR(arr, steps = 1) {
    let arrTmp = [...arr]; // clone to maintain clean references
    for (let i = 0; i < steps; i++) {
        arrTmp.unshift(arrTmp.pop());
    }
    return arrTmp;
}

// https://github.com/lodash/lodash/issues/2173#issuecomment-406597580
export function rotateL(arr, steps = 1) {
    let arrTmp = [...arr]; // clone to maintain clean references
    for (let i = 0; i < steps; i++) {
        arrTmp.push(arrTmp.shift());
    }
    return arrTmp;
}

// https://stackoverflow.com/questions/3746725/how-to-create-an-array-containing-1-n
export function filledArray(n) {
    return Array.from(Array(n).keys());
}

export function addToStrgArray(key, value) {
    let saved = localStorage.getItem(key);

    if (!saved) {
        localStorage.setItem(key, '[]');
        saved = [];
    } else {
        saved = JSON.parse(saved);
    }

    if (saved.includes(value)) return;

    saved.push(value);
    localStorage.setItem(key, JSON.stringify(saved));
}