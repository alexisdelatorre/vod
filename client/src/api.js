import axios from "axios";
import {useEffect, useState} from "react";

let localUrl = 'http://localhost:3001';

async function fetchAll() {
    let url = 'https://demo5520281.mockable.io/movies';
    let res = (await axios.get(url)).data.entries;
    return res.map(r => {
        return {
            // The api doesnt have ids so i am improvising
            id: r.title.toLowerCase().split(' ').join('_'),
            title: r.title,
            description: r.description,
            poster: r.images[0].url,
            ratings: r.parentalRatings.map(r => r.rating),
            audios: r.audios,
            credits: r.credits,
            video: r.contents[0].url,
        }
    });
}

async function fetchById(id) {
    let movies = await fetchAll();
    let movie = movies.filter(m => m.id === id)[0];
    if (!movie) throw new Error(`Movie not found: ${id}`);
    return movie;
}

function useMovie(id) {
    let [movie, setMovie] = useState({});
    let [loading, setLoading] = useState(true);

    useEffect(() => {
        fetchById(id).then(m => {
            setMovie(m);
            setLoading(false);
        });
    }, [id]);

    return [movie, loading];
}

async function setWatched(id, user) {
    await axios.post(localUrl + '/watched', { id, user });
}

async function fetchWatched(user) {
    return (await axios.get(`${localUrl}/watched/${user}`)).data;
}

async function createUser(username, password) {
    await axios.post(localUrl + '/user', { username, password });
}

async function login(username, password) {
    return (await axios.post(localUrl + '/login', { username, password })).data;
}

export default {
    fetchAll,
    fetchById,
    useMovie,
    setWatched,
    createUser,
    fetchWatched,
    login,
}