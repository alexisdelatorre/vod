const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const { Client } = require('pg');

let app = express();

let pg = new Client({
    user: process.env.PG_USER,
    password: process.env.PG_PW,
    database: process.env.PG_DB,
});
pg.connect();

app.use(cors());
app.use(bodyParser());

app.post('/login', async (req, res) => {
    let { username, password } = req.body;

    let userId = (await pg.query('select name from public.user where name = $1 and password = md5($2)', [username, password]))
        .rows.map(x => x.name)[0];

    if (!userId) {
        res.statusCode = 401;
        res.send('Wrong username or password');
    } else {
        res.send(userId.toString());
    }

});

app.post('/user', async (req, res) => {
    let { username, password } = req.body;

    let exists = (await pg.query('select * from public.user where name = $1', [username])).rows.length > 0;

    if (!exists) {
        await pg.query('insert into public.user (name, password) values ($1, md5($2))', [username, password]);
        res.send(200);
    }
    else {
        res.statusCode = 400;
        res.send('Username already taken');
    }
});

app.get('/watched/:user', async (req, res) => {
    let { user } = req.params;
    let movies = (await pg.query('select movie_id from watched where user_id = $1', [user])).rows.map(x => x['movie_id']);
    res.send(movies);
});

app.post('/watched', async (req, res) => {
    let { id, user } = req.body;
    let exists = (await pg.query('select * from watched where user_id = $1 and movie_id = $2', [user, id])).rows.length > 0;
    if (!exists) await pg.query('insert into watched (user_id, movie_id) values ($1,$2)', [user, id]);
});

app.listen(3001);
